$(document).foundation();

// @koala-append "jqxcore.js"
// @koala-append "jqxdraw.js"
// @koala-append "jqxknob.js"
// @koala-append "cycle.js"

PlantPro = {};

PlantPro.dial = function(container, val, mn, mx, startangle, endangle, stroke, color, r, g, b, remaining, progress, barstyle, barbackground) {
	$('#' + container).jqxKnob({
		value : val,
		min : mn,
		max : mx,
		startAngle : startangle,
		endAngle : endangle,
		width: '360%',
        height: '360%',
        snapToStep: true,
        disabled: true,
        rotation: 'clockwise',
        style: { 
            stroke: stroke, 
            strokeWidth: 0, 
            fill: { 
                color: color, 
                gradientType: "linear", 
                gradientStops: [[r, r], [g, g], [b, b]] 
            } 
        },
        marks: {
            colorRemaining: { color: remaining, border: remaining },
            colorProgress: { color: progress, border: progress },
            type: 'line',
            offset: '97%',
            thickness: 1,
            size: '2%',
            majorSize: '3%',
            majorInterval: 2,
            minorInterval: 1
        },
        labels: {
            offset: '88%',
            step: 3,
            visible: false
        },
        progressBar: {
            style: { fill: barstyle, stroke: barstyle },
            size: '35%',
            offset: '60%',
            background: { fill: barbackground, stroke: barbackground }
        },
        pointer: {  }
	});
};

$(function() {
	var $window = $(window);
	var size	= Foundation.MediaQuery.current;

	equalHeight = function(container, height) {
		var $el = $(container),
			height,
			$target = $('.grid-height'),
			maxHeight = 0;


		$el.each(function() {
			thisHeight = $(this).height();

			if (thisHeight > maxHeight) {
				maxHeight = thisHeight;
			}
		});

		if (maxHeight) {
			$target.height(maxHeight);
		}
		else {
			$target.height('auto');
		}
	};

	$window.on('load resize', function() {
		var width = $window.width();

		if (Foundation.MediaQuery.atLeast('large')) {
			//equalHeight('.grid-column', true);
		}
		else {
			//equalHeight('.grid-column', false);
		}
	});

	var isiPad = navigator.userAgent.match(/iPad/i) != null;

	if (isiPad) {
		$('html').addClass('tablet');
	}
});