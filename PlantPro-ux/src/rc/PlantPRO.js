define([], function () {
  'use strict';

  /**
   * Yarr 'tis a module
   *
   * @exports nmodule/PlantPRO/rc/PlantPRO
   */
  var PlantPRO = {};

  /**
   * Extol the virtues of PlantPRO.
   *
   * @returns {string}
   */
  PlantPRO.extolVirtues = function () {
    return 'PlantPRO is great!';
  };

  return PlantPRO;
});