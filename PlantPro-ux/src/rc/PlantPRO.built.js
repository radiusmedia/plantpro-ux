
define('nmodule/PlantPRO/rc/PlantPRO',[], function () {
  

  /**
   * Yarr 'tis a module
   *
   * @exports nmodule/PlantPRO/rc/PlantPRO
   */
  var PlantPRO = {};

  /**
   * Extol the virtues of PlantPRO.
   *
   * @returns {string}
   */
  PlantPRO.extolVirtues = function () {
    return 'PlantPRO is great!';
  };

  return PlantPRO;
});

/* START_TEMPLATE */
define('hbs!nmodule/PlantPRO/rc/template/PlantPROWidget-structure',['hbs','Handlebars'], function( hbs, Handlebars ){ 
var t = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "\n<div class=\"PlantPROWidget\">\n  <div class=\"PlantPROWidget-header\">\n    <p>\n      "
    + escapeExpression(((helper = (helper = helpers.titleText || (depth0 != null ? depth0.titleText : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"titleText","hash":{}}) : helper)))
    + "\n    </p>\n    <p>\n      <span class=\"PlantPROWidget-selected-slot-text\">"
    + escapeExpression(((helper = (helper = helpers.selectedSlotText || (depth0 != null ? depth0.selectedSlotText : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"selectedSlotText","hash":{}}) : helper)))
    + "</span>\n      <span class=\"PlantPROWidget-selected-slot\"></span>\n    </p>\n  </div>\n  <div class=\"PlantPROWidget-content\"></div>\n</div>\n";
}});
Handlebars.registerPartial('nmodule/PlantPRO/rc/template/PlantPROWidget-structure', t);
return t;
});
/* END_TEMPLATE */
;
/* START_TEMPLATE */
define('hbs!nmodule/PlantPRO/rc/template/PlantPROWidget-content',['hbs','Handlebars'], function( hbs, Handlebars ){ 
var t = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"PlantPROWidget-button-container\">\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.buttons : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>\n";
},"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "    <button class=\"PlantPROWidget-button\" type=\"button\" data-slot=\""
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{}}) : helper)))
    + "\">\n      "
    + escapeExpression(((helper = (helper = helpers.displayName || (depth0 != null ? depth0.displayName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"displayName","hash":{}}) : helper)))
    + "\n    </button>\n";
}});
Handlebars.registerPartial('nmodule/PlantPRO/rc/template/PlantPROWidget-content', t);
return t;
});
/* END_TEMPLATE */
;

/**
 * A module defining `PlantPROWidget`.
 *
 * @module nmodule/PlantPRO/rc/PlantPROWidget
 */
define('nmodule/PlantPRO/rc/PlantPROWidget',['nmodule/webEditors/rc/fe/baja/BaseEditor',
        'bajaux/mixin/subscriberMixIn',
        'jquery',
        'Promise',
        'hbs!nmodule/PlantPRO/rc/template/PlantPROWidget-structure',
        'hbs!nmodule/PlantPRO/rc/template/PlantPROWidget-content',
        'css!nmodule/PlantPRO/rc/PlantPRO'], function (
        BaseEditor,
        subscriberMixin,
        $,
        Promise,
        tplPlantPROWidgetStructure,
        tplPlantPROWidgetContent) {

  

  var SELECTED_CLASS = 'active',
      BUTTON_CLASS = 'PlantPROWidget-button';

  /**
   * A demonstration Widget. This builds a list of buttons from the slots of a
   * Complex value, allowing the user to select a slot.
   *
   * @class
   * @extends module:nmodule/webEditors/rc/fe/baja/BaseEditor
   * @alias module:nmodule/PlantPRO/rc/PlantPROWidget
   */
  var PlantPROWidget = function PlantPROWidget() {
    /** remember to call super constructor. Javascript won't do this for you */
    BaseEditor.apply(this, arguments);
    subscriberMixin(this);
  };

  //extend and set up prototype chain
  PlantPROWidget.prototype = Object.create(BaseEditor.prototype);
  PlantPROWidget.prototype.constructor = PlantPROWidget;

  /**
   * Do initial setup of the DOM for the widget. This will set up the DOM's
   * structure and create a space where the buttons will go.
   *
   * @param {jQuery} element the DOM element into which to load this widget
   */
  PlantPROWidget.prototype.doInitialize = function (dom) {
    var that = this;

    dom.html(tplPlantPROWidgetStructure({
      titleText: "These are the slots on your component.",
      selectedSlotText: "You've selected slot: "
    }));

    dom.on('click', '.PlantPROWidget-content button', function () {
      var $this = $(this);
      $this.siblings().removeClass(SELECTED_CLASS);
      $this.addClass(SELECTED_CLASS);
      that.$updateSlotText();
      that.setModified(true);
    });
  };

  /**
   * Reads the currently selected slot and update the display accordingly.
   * The display will be updated asynchronously.
   *
   * @private
   * @returns {Promise}
   */
  PlantPROWidget.prototype.$updateSlotText = function () {
    var that = this;

    return that.read().then(function (slotName) {
      that.jq().find('.PlantPROWidget-selected-slot').text(slotName);
    });
  };

  /**
   * Builds the actual buttons and loads them into the widget.
   *
   * @private
   * @param {baja.Complex} value the value being loaded in
   */
  PlantPROWidget.prototype.$buildButtons = function (value) {
    var that = this,
        dom = that.jq(),
        contentDom = dom.find('.PlantPROWidget-content'),
        buttons = [];

    value.getSlots().each(function (slot) {
      buttons.push({
        name: slot.getName(),
        displayName: value.getDisplayName(slot)
      });
    });

    contentDom.html(tplPlantPROWidgetContent({
      buttons: buttons
    }));

    that.$updateSlotText();
  };

  /**
   * Loads in a Complex value and builds up an array of buttons, one for each
   * slot.
   *
   * @param {baja.Complex} value the complex value whose slots you wish to
   * select from
   */
  PlantPROWidget.prototype.doLoad = function (value) {
    var that = this;

    that.$buildButtons(value);

    that.getSubscriber().attach('added removed renamed', function () {
      that.$buildButtons(value);
    });
  };

  /**
   * Gets the currently selected slot
   *
   * @returns {Promise} promise to be resolved with the name of the currently
   * selected slot
   */
  PlantPROWidget.prototype.doRead = function () {
    var selectedButton = this.jq().find(
          '.PlantPROWidget-content .' + BUTTON_CLASS + '.' + SELECTED_CLASS);

    //promises are optional - the slot could also be returned directly
    return Promise.resolve(selectedButton.data('slot'));
  };

  return PlantPROWidget;
});

/* START_TEMPLATE */
define('hbs!nmodule/PlantPRO/rc/template/PlantPRO',['hbs','Handlebars'], function( hbs, Handlebars ){ 
var t = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<p>\n  Allow me to extol its virtues for a moment:\n</p>\n<div class=\"virtues\">\n  <h3>"
    + escapeExpression(((helper = (helper = helpers.virtues || (depth0 != null ? depth0.virtues : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"virtues","hash":{}}) : helper)))
    + "</h3>\n</div>\n";
}});
Handlebars.registerPartial('nmodule/PlantPRO/rc/template/PlantPRO', t);
return t;
});
/* END_TEMPLATE */
;

require(['baja!',
         'bajaux/events',
         'nmodule/PlantPRO/rc/PlantPRO',
         'jquery',
         'nmodule/PlantPRO/rc/PlantPROWidget',
         'hbs!nmodule/PlantPRO/rc/template/PlantPRO'], function (
         baja,
         events,
         PlantPRO,
         $,
         PlantPROWidget,
         template) {

  

  $("#template").html(template({
    virtues: PlantPRO.extolVirtues()
  }));

  var widget = new PlantPROWidget(),
      comp = baja.$('baja:Component', {
        'meritorious': true,
        'renowned': true,
        'sublime': true,
        'whimsical': true,
        'transcendent': true
      });

  var widgetDiv = $('#widget'),
      description = $('#description');

  widget.initialize(widgetDiv)
    .then(function () {

      widgetDiv.on(events.MODIFY_EVENT, function () {
        widget.read()
          .then(function (value) {
            description.text(value);
          });
      });

      return widget.load(comp);
    });
});


define("nmodule/PlantPRO/rc/PlantPRO.run", function(){});
